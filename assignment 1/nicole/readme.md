# steps taken

I used parts of an old report from a project of Piraeus Bank for this assignment. The part were saved in the file:

`report.odt`

## first part

Using only the basic html elements, i created the `report.html` file. The elements I used were headings, paragraph and an image. I did not go into styling.

### Issue

Greek language is not recognized, even though the tag `lang='el'` was added. I will need to investigate it further.

#### Solution

I added the following command, in the head of the document:

    <meta http-equiv="content-type" content="text/html;charset=utf-8" />

I found the above information in the article [Get HTML Codes for Greek Language Characters ](https://www.lifewire.com/html-codes-greek-characters-4062212).

Following @qwazix's input I removed the above and added:

    <meta charset="utf-8">

Greek language was recognized. I checked to see if the 

    <html lang="el">

command was necessary. It was not. 

## second part

Using LibreOffice, I saved the report in html form ( `reportLibO.html` ). The document was well presented in firefox. I looked into the file to check the html syntax. Interesting things I noticed:

 * the use of this line <meta http-equiv="content-type" content="text/html;charset=utf-8" /> for characters recongition.
 * all the meta information
 * the styling information that included in the .css part of the file

