# Project Scope

To create an interactive training environment where anyone, interested in gaining knowledge on web design and development, can participate.

An important part of understanding web development is to know the history of the web and web design trends in particular.

## Some interesting links to read are the following

[A brief history of web design - all parts](http://www.outofbounds.gr/blog/posts/a-brief-history-of-web-design)

[World Wide Web - wikipedia page](https://en.wikipedia.org/wiki/World_Wide_Web)

# Rules

The project is public. Whoever wants to participate must fork the project and work on his own branch. 

Answers to exercises will be submitted as *merge requests*.

